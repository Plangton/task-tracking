class TaskController < ApplicationController
  def list
      @tasks = Task.all
  end
  
  def show
      @task=Task.find(params[:id])
  end
  
  def new
      @task=Task.new
      @users=User.all
  end
  
  def create
     @task = Task.new(task_params)
  	 @task.creation_date = Time.now
     if @task.save
        redirect_to :action => 'list'
     else
        @users = User.all
        render :action => 'new'
     end
     
  end
  
  def task_params
     params.require(:tasks).permit(:name, :user_id, :end_date, :description)
  end
  
  def edit
      @task=Task.find(params[:id])
      @users=User.all
  end
  
  def update
   @task = Task.find(params[:id])
	
   if @task.update_attributes(task_param)
      redirect_to :action => 'show', :id => @task
   else
      @users = User.all
      render :action => 'edit'
   end
   
  end

  def task_param
     params.require(:task).permit(:name, :creation_date, :end_date, :description, :user_id)
  end
  
  def delete
      @task = Task.find(params[:id]).destroy
      redirect_to :action => 'list'
  end

  def show_users
     @user = User.find(params[:id])
  end
end
