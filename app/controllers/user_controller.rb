class UserController < ApplicationController
  def list
      @users = User.all
  end
  
  def show
      @user = User.find(params[:id])
  end

  def new
      @user=User.new
  end
  
  def create
     @user = User.new(user_params)
  	
     if @user.save
        redirect_to :action => 'list'
     else
        render :action => 'new'
     end
     
  end
  
  def user_params
     params.require(:users).permit(:name, :family)
  end
  
  def edit
      @user=User.find(params[:id])
  end
  
  def update
   @user = User.find(params[:id])
	
   if @user.update_attributes(user_param)
      redirect_to :action => 'show', :id => @user
   else
      render :action => 'edit'
   end
   
  end

  def task_param
     params.require(:tasks).permit(:name, :family)
  end
  
  def delete
      @user = User.find(params[:id]).destroy
      redirect_to :action => 'list'
  end

end
