class Users < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
       t.column :name, :string, :limit => 32, :null => false
       t.column :family, :string, :limit => 32
    end
  end

   def self.down
      drop_table :users
   end
end
