class Tasks < ActiveRecord::Migration
      def self.up
      create_table :tasks do |t|
         t.column :name, :string, :limit => 32, :null => false
         t.column :creation_date, :datetime
         t.column :end_date, :datetime
         t.column :description, :text
         t.column :user_id, :integer
      end
   end

   def self.down
      drop_table :tasks
   end
end
